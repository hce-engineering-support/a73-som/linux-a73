// SPDX-License-Identifier: (GPL-2.0+ OR BSD-3-Clause)
/*
 * Copyright (C) Rodolfo Giometti <giometti@hce-engineering.it>
 * Author: Rodolfo Giometti <giometti@hce-engineering.it> for HCE Engineering
 */

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/mfd/st,stpmic1.h>

/ {
	model = "HCE Engineeering A73 Board";
	compatible = "hce,a73", "st,stm32mp157";

	aliases {
		ethernet0 = &ethernet0;
		serial0 = &uart4;
		serial1 = &usart1;
		serial2 = &usart3;
		serial3 = &usart6;
		serial4 = &uart8;
	};

	memory@c0000000 {
		device_type = "memory";
		reg = <0xc0000000 0x20000000>;
	};

	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		mcuram2: mcuram2@10000000 {
			compatible = "shared-dma-pool";
			reg = <0x10000000 0x40000>;
			no-map;
		};

		vdev0vring0: vdev0vring0@10040000 {
			compatible = "shared-dma-pool";
			reg = <0x10040000 0x1000>;
			no-map;
		};

		vdev0vring1: vdev0vring1@10041000 {
			compatible = "shared-dma-pool";
			reg = <0x10041000 0x1000>;
			no-map;
		};

		vdev0buffer: vdev0buffer@10042000 {
			compatible = "shared-dma-pool";
			reg = <0x10042000 0x4000>;
			no-map;
		};

		mcuram: mcuram@30000000 {
			compatible = "shared-dma-pool";
			reg = <0x30000000 0x40000>;
			no-map;
		};

		retram: retram@38000000 {
			compatible = "shared-dma-pool";
			reg = <0x38000000 0x10000>;
			no-map;
		};

		gpu_reserved: gpu@d4000000 {
			reg = <0xd4000000 0x4000000>;
			no-map;
		};
	};

        v3v3: regulator-v3v {
                compatible = "regulator-fixed";
                regulator-name = "v3v3";
                regulator-min-microvolt = <3300000>;
                regulator-max-microvolt = <3300000>;
                regulator-always-on;
	};

        vdd: regulator-vdd {
                compatible = "regulator-fixed";
                regulator-name = "vdd";
                regulator-min-microvolt = <3300000>;
                regulator-max-microvolt = <3300000>;
                regulator-always-on;
	};

        vdd_usb: regulator-vdd-usb {
		compatible = "regulator-fixed";
                regulator-name = "vdd_usb";
                regulator-min-microvolt = <3300000>;
                regulator-max-microvolt = <3300000>;
		regulator-always-on;
	};
};

&pinctrl {
        ethernet0_rmii_pins_sleep_a: rmii-sleep-0 {
                pins1 {
                        pinmux = <STM32_PINMUX('B', 12, ANALOG)>, /* ETH1_RMII_TXD0 */
                                 <STM32_PINMUX('B', 13, ANALOG)>, /* ETH1_RMII_TXD1 */
                                 <STM32_PINMUX('B', 11, ANALOG)>, /* ETH1_RMII_TX_EN */
                                 <STM32_PINMUX('A', 1, ANALOG)>,  /* ETH1_RMII_REF_CLK */
                                 <STM32_PINMUX('A', 2, ANALOG)>,  /* ETH1_MDIO */
                                 <STM32_PINMUX('C', 1, ANALOG)>,  /* ETH1_MDC */
                                 <STM32_PINMUX('C', 4, ANALOG)>,  /* ETH1_RMII_RXD0 */
                                 <STM32_PINMUX('C', 5, ANALOG)>,  /* ETH1_RMII_RXD1 */
                                 <STM32_PINMUX('A', 7, ANALOG)>;  /* ETH1_RMII_CRS_DV */
                };
        };

        usart3_pins_a: usart3-0 {
                pins1 {
                        pinmux = <STM32_PINMUX('D', 8, AF7)>; /* USART3_TX */
                        bias-disable;
                        drive-push-pull;
                        slew-rate = <0>;
                };
                pins2 {
                        pinmux = <STM32_PINMUX('D', 9, AF7)>, /* USART3_RX */
				 <STM32_PINMUX('D', 11, AF7)>, /* USART3_CTS */
				 <STM32_PINMUX('D', 12, AF7)>; /* USART3_RTS */
                        bias-disable;
                };
        };

        usart6_pins_a: usart6-0 {
                pins1 {
                        pinmux = <STM32_PINMUX('C', 6, AF7)>; /* USART6_TX */
                        bias-disable;
                        drive-push-pull;
                        slew-rate = <0>;
                };
                pins2 {
                        pinmux = <STM32_PINMUX('C', 7, AF7)>, /* USART6_RX */
				 <STM32_PINMUX('G', 13, AF7)>, /* USART6_CTS */
				 <STM32_PINMUX('G', 8, AF7)>; /* USART6_RTS */
                        bias-disable;
                };
        };

        uart8_pins_a: uart8-0 {
                pins1 {
                        pinmux = <STM32_PINMUX('E', 1, AF8)>; /* UART8_TX */
                        bias-disable;
                        drive-push-pull;
                        slew-rate = <0>;
                };
                pins2 {
                        pinmux = <STM32_PINMUX('E', 0, AF8)>, /* UART8_RX */
				 <STM32_PINMUX('D', 15, AF8)>, /* UART8_CTS */
				 <STM32_PINMUX('G', 7, AF8)>; /* UART8_RTS */
                        bias-disable;
                };
        };

        i2c3_pins_a: i2c3-0 {
                pins {
                        pinmux = <STM32_PINMUX('H', 7, AF4)>, /* I2C4_SCL */
                                 <STM32_PINMUX('H', 8, AF4)>; /* I2C4_SDA */
                        bias-disable;
                        drive-open-drain;
                        slew-rate = <0>;
                };
        };

        i2c3_sleep_pins_a: i2c3-sleep-0 {
                pins {
                        pinmux = <STM32_PINMUX('H', 7, ANALOG)>, /* I2C4_SCL */
                                 <STM32_PINMUX('H', 8, ANALOG)>; /* I2C4_SDA */
                };
        };

        i2c4_pins_b: i2c4-0 {
                pins {
                        pinmux = <STM32_PINMUX('H', 11, AF4)>, /* I2C4_SCL */
                                 <STM32_PINMUX('H', 12, AF4)>; /* I2C4_SDA */
                        bias-disable;
                        drive-open-drain;
                        slew-rate = <0>;
                };
        };

        i2c4_sleep_pins_b: i2c4-sleep-0 {
                pins {
                        pinmux = <STM32_PINMUX('H', 11, ANALOG)>, /* I2C4_SCL */
                                 <STM32_PINMUX('H', 12, ANALOG)>; /* I2C4_SDA */
                };
        };
};

&pinctrl_z {
        usart1_pins_a: usart1-0 {
                pins1 {
                        pinmux = <STM32_PINMUX('Z', 7, AF7)>; /* USART1_TX */
                        bias-disable;
                        drive-push-pull;
                        slew-rate = <0>;
                };
                pins2 {
                        pinmux = <STM32_PINMUX('Z', 1, AF7)>, /* USART1_RX */
				 <STM32_PINMUX('Z', 3, AF7)>, /* USART1_CTS */
				 <STM32_PINMUX('Z', 5, AF7)>; /* USART1_RTS */
                        bias-disable;
                };
        };
};

&adc {
	pinctrl-names = "default";
	pinctrl-0 = <&adc12_ain_pins_a>, <&adc12_usb_cc_pins_a>;
	vdd-supply = <&vdd>;
	vdda-supply = <&vdd>;
	vref-supply = <&vrefbuf>;
	status = "disabled";
	adc1: adc@0 {
		/*
		 * Type-C USB_PWR_CC1 & USB_PWR_CC2 on in18 & in19.
		 * Use at least 5 * RC time, e.g. 5 * (Rp + Rd) * C:
		 * 5 * (56 + 47kOhms) * 5pF => 2.5us.
		 * Use arbitrary margin here (e.g. 5us).
		 */
		st,min-sample-time-nsecs = <5000>;
		/* AIN connector, USB Type-C CC1 & CC2 */
		st,adc-channels = <0 1 6 13 18 19>;
		status = "okay";
	};
	adc2: adc@100 {
		/* AIN connector, USB Type-C CC1 & CC2 */
		st,adc-channels = <0 1 2 6 18 19>;
		st,min-sample-time-nsecs = <5000>;
		status = "okay";
	};
};

&cec {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&cec_pins_b>;
	pinctrl-1 = <&cec_sleep_pins_b>;
	status = "okay";
};

&crc1 {
	status = "okay";
};

&dts {
	status = "okay";
};

&ethernet0 {
	status = "okay";
	interrupts-extended = <&intc GIC_SPI 61 IRQ_TYPE_LEVEL_HIGH>,
			      <&intc GIC_SPI 62 IRQ_TYPE_LEVEL_HIGH>,
			      <&exti 70 1>;
	interrupt-names = "macirq",
			  "eth_wake_irq",
			  "stm32_pwr_wakeup";

	pinctrl-0 = <&ethernet0_rmii_pins_a>;
	pinctrl-1 = <&ethernet0_rmii_pins_sleep_a>;
	pinctrl-names = "default", "sleep";
	phy-mode = "rmii";
	max-speed = <100>;
	phy-handle = <&phy0>;

	mdio0 {
		#address-cells = <1>;
		#size-cells = <0>;
		compatible = "snps,dwmac-mdio";
		reset-gpios = <&gpioh 6 GPIO_ACTIVE_LOW>;
		reset-delay-us = <1000>;

		phy0: ethernet-phy@1 {
			reg = <1>;
		};
	};
};

&gpu {
	contiguous-area = <&gpu_reserved>;
	status = "okay";
};

&hash1 {
	status = "okay";
};

&i2c2 {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c2_pins_a>;
	pinctrl-1 = <&i2c2_sleep_pins_a>;
	i2c-scl-rising-time-ns = <100>;
	i2c-scl-falling-time-ns = <7>;
	status = "okay";
	/delete-property/dmas;
	/delete-property/dma-names;

	pcf8563: pcf8563@51 {
		compatible = "nxp,pcf8563";
		reg = <0x51>;
		status = "okay";
	};
};

&i2c3 {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c3_pins_a>;
	pinctrl-1 = <&i2c3_sleep_pins_a>;
	i2c-scl-rising-time-ns = <100>;
	i2c-scl-falling-time-ns = <7>;
	status = "okay";
	/delete-property/dmas;
	/delete-property/dma-names;
};

&i2c4 {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c4_pins_b>;
	pinctrl-1 = <&i2c4_sleep_pins_b>;
	i2c-scl-rising-time-ns = <185>;
	i2c-scl-falling-time-ns = <20>;
	clock-frequency = <400000>;
	status = "okay";
	/delete-property/dmas;
	/delete-property/dma-names;
};

&ipcc {
	status = "okay";
};

&iwdg2 {
	timeout-sec = <32>;
	status = "okay";
};

&m4_rproc {
	memory-region = <&retram>, <&mcuram>, <&mcuram2>, <&vdev0vring0>,
			<&vdev0vring1>, <&vdev0buffer>;
	mboxes = <&ipcc 0>, <&ipcc 1>, <&ipcc 2>, <&ipcc 3>;
	mbox-names = "vq0", "vq1", "shutdown", "detach";
	interrupt-parent = <&exti>;
	interrupts = <68 1>;
	status = "okay";
};

&pwr_regulators {
	vdd-supply = <&vdd>;
	vdd_3v3_usbfs-supply = <&vdd_usb>;
};

&cryp1 {
	status = "okay";
};

&rng1 {
	status = "okay";
};

&rtc {
	status = "okay";
};

&sdmmc1 {
	pinctrl-names = "default", "opendrain", "sleep";
	pinctrl-0 = <&sdmmc1_b4_pins_a>;
	pinctrl-1 = <&sdmmc1_b4_od_pins_a>;
	pinctrl-2 = <&sdmmc1_b4_sleep_pins_a>;
	broken-cd; /* cd-gpios = <&gpioe 4 (GPIO_ACTIVE_LOW | GPIO_PULL_UP)>; */
	disable-wp;
	/delete-property/ st,use-ckin;
        /delete-property/ sd-uhs-sdr12;
	/delete-property/ sd-uhs-sdr25;
	/delete-property/ sd-uhs-sdr50;
	/delete-property/ sd-uhs-ddr50;
	st,neg-edge;
	bus-width = <4>;
	vmmc-supply = <&v3v3>;
	/delete-property/ vqmmc-supply;
	status = "okay";
};

&sdmmc2 {
        pinctrl-names = "default", "opendrain", "sleep";
        pinctrl-0 = <&sdmmc2_b4_pins_b>;
        pinctrl-1 = <&sdmmc2_b4_od_pins_b>;
        pinctrl-2 = <&sdmmc2_b4_sleep_pins_a>;
        non-removable;
        st,neg-edge;
        bus-width = <4>;
        vmmc-supply = <&v3v3>;
        status = "okay";
};

&timers2 {
        /* spare dmas for other usage (un-delete to enable pwm capture) */
        /delete-property/dmas;
        /delete-property/dma-names;
        status = "okay";
        backlight_pwm: pwm {
                pinctrl-0 = <&pwm2_pins_a>;
                pinctrl-1 = <&pwm2_sleep_pins_a>;
                pinctrl-names = "default", "sleep";
                status = "okay";
        };
        timer@1 {
                status = "okay";
        };
};

&timers3 {
	/delete-property/dmas;
	/delete-property/dma-names;
	status = "disabled";
	pwm {
		pinctrl-0 = <&pwm3_pins_a>;
		pinctrl-1 = <&pwm3_sleep_pins_a>;
		pinctrl-names = "default", "sleep";
		status = "okay";
	};
	timer@2 {
		status = "okay";
	};
};

&timers4 {
	/delete-property/dmas;
	/delete-property/dma-names;
	status = "disabled";
	pwm {
		pinctrl-0 = <&pwm4_pins_a &pwm4_pins_b>;
		pinctrl-1 = <&pwm4_sleep_pins_a &pwm4_sleep_pins_b>;
		pinctrl-names = "default", "sleep";
		status = "okay";
	};
	timer@3 {
		status = "okay";
	};
};

&timers5 {
	/delete-property/dmas;
	/delete-property/dma-names;
	status = "disabled";
	pwm {
		pinctrl-0 = <&pwm5_pins_a>;
		pinctrl-1 = <&pwm5_sleep_pins_a>;
		pinctrl-names = "default", "sleep";
		status = "okay";
	};
	timer@4 {
		status = "okay";
	};
};

&timers6 {
	/delete-property/dmas;
	/delete-property/dma-names;
	status = "disabled";
	timer@5 {
		status = "okay";
	};
};

&timers12 {
	/delete-property/dmas;
	/delete-property/dma-names;
	status = "disabled";
	pwm {
		pinctrl-0 = <&pwm12_pins_a>;
		pinctrl-1 = <&pwm12_sleep_pins_a>;
		pinctrl-names = "default", "sleep";
		status = "okay";
	};
	timer@11 {
		status = "okay";
	};
};

&usart1 {
        pinctrl-names = "default";
        pinctrl-0 = <&usart1_pins_a>;
        linux,rs485-enabled-at-boot-time;
        rs485-rts-delay = <0 20>;
        status = "okay";
};

&usart3 {
        pinctrl-names = "default";
        pinctrl-0 = <&usart3_pins_a>;
        status = "okay";
};

&uart4 {
	pinctrl-names = "default", "sleep", "idle";
	pinctrl-0 = <&uart4_pins_a>;
	pinctrl-1 = <&uart4_sleep_pins_a>;
	pinctrl-2 = <&uart4_idle_pins_a>;
	status = "okay";
};

&usart6 {
        pinctrl-names = "default";
	pinctrl-0 = <&usart6_pins_a>;
	status = "disabled";
};

&uart8 {
        pinctrl-names = "default";
	pinctrl-0 = <&uart8_pins_a>;
        linux,rs485-enabled-at-boot-time;
        rs485-rts-delay = <0 20>;
	status = "okay";
};

&usbh_ehci {
	phys = <&usbphyc_port0>, <&usbphyc_port1 1>;
	phy-names = "usb", "usb";
	status = "okay";
};

&usbh_ohci {
	phys = <&usbphyc_port0>, <&usbphyc_port1 1>;
	phy-names = "usb", "usb";
	status = "okay";
};

&usbphyc {
	status = "okay";
};

&usbphyc_port0 {
	phy-supply = <&vdd_usb>;
	vdda1v1-supply = <&reg11>;
	vdda1v8-supply = <&reg18>;
};

&usbphyc_port1 {
	phy-supply = <&vdd_usb>;
	vdda1v1-supply = <&reg11>;
	vdda1v8-supply = <&reg18>;
};

&vrefbuf {
	regulator-min-microvolt = <2500000>;
	regulator-max-microvolt = <2500000>;
	vdda-supply = <&vdd>;
	status = "okay";
};
